# Plugin Settings
export ZSH_PLUGINS_ALIAS_TIPS_TEXT=$'\u2192'" Alias: "

# Setup terminal, and turn on colors
export TERM="xterm-256color"
export CLICOLOR=1
#export LSCOLORS=Exfxcxdxbxegedabagacad

export EDITOR=vim

# brew install zsh-completions
fpath=(/usr/local/share/zsh-completions $fpath)
