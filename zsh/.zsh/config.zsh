# HISTORY
HISTSIZE=10000
SAVEHIST=9000
HISTFILE=~/.zsh_history
HISTCONTROL=ignoredups:erasedups
HISTIGNORE="&:ls:[bf]g:exit:pwd:clear:mount:umount:[ \t]*"

# Powerlevel9k Theme Settings
# https://github.com/bhilburn/powerlevel9k
POWERLEVEL9K_INSTALLATION_PATH=$ANTIGEN_BUNDLES/bhilburn/powerlevel9k.zsh-theme
POWERLEVEL9K_MODE='nerdfont-complete'
POWERLEVEL9K_COLOR_SCHEME='light'
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon dir dir_writable root_indicator background_jobs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(ssh virtualenv go_version nodeenv nvm pyenv chruby rspec_stats rbenv vcs status history time todo)
POWERLEVEL9K_PROMPT_ADD_NEWLINE=true
POWERLEVEL9K_TIME_FORMAT="%D{\uF017 %H:%M}"
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_SHORTEN_DELIMITER=""
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
POWERLEVEL9K_LEFT_SEGMENT_SEPARATOR="\uE0B4"
# POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR=""
POWERLEVEL9K_RIGHT_SEGMENT_SEPARATOR="\uE0B6"
# POWERLEVEL9K_RIGHT_SUBSEGMENT_SEPARATOR=""

# ZSTYLE
ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)
zstyle ':notify:*' error-icon "https://d.pr/i/xpaTDv+"
zstyle ':notify:*' error-title "FAIL"
zstyle ':notify:*' success-icon "https://d.pr/i/UYgnwM+"
zstyle ':notify:*' success-title "Success"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
