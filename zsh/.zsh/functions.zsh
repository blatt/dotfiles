# Auto tmux session on ssh connection
# function ssh () {/usr/bin/ssh -t $@ "tmux a || tmux";}

# Create a new directory and enter it
function mkd() {
    mkdir -p "$@" && cd "$@"
}

# add https://github.com/ttscoff/na
# [[ -s “$HOME/bin/na.sh” ]] && source “$HOME/na.sh”


