# Add todo.txt
export TODOTXT_DEFAULT_ACTION=ls
alias t='todo.sh -d ~/.todo/todo.cfg'

# installed with homebrew
alias dash="open dash://"
alias sf="screenfetch"
alias nf="neofetch"

# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias -- -="cd -"

alias cl='clear'
alias reload!='. ~/.zshrc'
#alias deploy!='surge' #-> Needs http://surge.sh
alias deploy!='netlify deploy'

# Shortcuts
alias nc="cd ~/Nextcloud"
alias dl="cd ~/Downloads"
alias dev="cd ~/Dev"
alias g="git"
alias h="history"

# Fun
alias cow="fortune -s -n 300 | cowsay | lolcat"
alias pony="fortune -s -n 300 | ponysay"

# Quick access to the ~/.zshrc file
alias zshrc='$EDITOR ~/.zshrc'
alias todo='$EDITOR TODO.txt'

#Homebrew
alias brewi='brew install '
alias brewu='brew update'
alias app-install='brew cask install '
alias app-search='brew cask search '

# List all files in long format
alias l="ls -lF}"

# List all files in long format, including dot files
alias la="ls -laF"

# List only directories
alias lsd="ls -lF | grep '^d'"

alias ls="ls -hAFG"

# List Dotfiles
alias l.='ls -A | grep '^\.''

# Copy public SSH Key to clipboard
alias sshkey='pbcopy < ~/.ssh/id_rsa.pub'

# Enable aliases to be sudo’ed
alias sudo='sudo '

# Move folder content in parent folder
alias move-up='mv * ../'

# Trim new lines and copy to clipboard
alias c="tr -d '\n' | pbcopy"

# Copy path to clipboard
alias cpath='pwd | pbcopy'

# Recursively delete `.DS_Store` files
alias cleanup="find . -type f -name '*.DS_Store' -ls -delete"

# Quicklook Files
alias ql="qlmanage -p"
alias edit='code'

# Update Homebrew
alias update-brew='brew update; brew upgrade --all; brew cleanup; brew doctor'
# Update Node npm
alias update-node='npm install npm -g; npm update -g'
# Update Ruby Gems
alias update-ruby='sudo gem update --system; sudo gem update'
# Get macOS Software Updates
alias update-mac='sudo softwareupdate -i -a'
# Get ALL Updates
alias update='sudo softwareupdate -i -a; brew update; brew upgrade --all; brew cleanup; npm install npm -g; npm update -g; sudo gem update --system; sudo gem update'

# View HTTP traffic
alias sniff="sudo ngrep -d 'en1' -t '^(GET|POST) ' 'tcp and port 80'"
alias httpdump="sudo tcpdump -i en1 -n -s 0 -w - | grep -a -o -E \"Host\: .*|GET \/.*\""
# Show ports
alias ports='netstat -tulanp'
# Stop after sending count ECHO_REQUEST packets
alias ping='ping -c 5'

# Canonical hex dump; some systems have this symlinked
command -v hd > /dev/null || alias hd="hexdump -C"

# OS X has no `md5sum`, so use `md5` as a fallback
command -v md5sum > /dev/null || alias md5sum="md5"

# OS X has no `sha1sum`, so use `shasum` as a fallback
command -v sha1sum > /dev/null || alias sha1sum="shasum"

# Make zsh know about hosts already accessed by SSH
zstyle -e ':completion:*:(ssh|scp|sftp|rsh|rsync):hosts' hosts 'reply=(${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) /dev/null)"}%%[# ]*}//,/ })'
