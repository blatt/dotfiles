# # My .zshrc dotfile setup uses Antigen
# # https://github.com/zsh-users/antigen

# Add Antigen
source ~/.zsh/antigen/antigen.zsh

# Add Custom Fonts
source ~/.fonts/*.sh
# Source Configs
source ~/.zsh/exports.zsh
source ~/.zsh/functions.zsh
source ~/.zsh/aliases.zsh
source ~/.zsh/secrets.zsh

# Load robbyrussell's oh-my-zsh's library.
antigen use oh-my-zsh

# Load the theme.
antigen theme bhilburn/powerlevel9k powerlevel9k

# Add Bundles
antigen bundles <<EOBUNDLES

# Development
git
pip
python
virtualenv
vagrant
voronkovich/get-jquery.plugin.zsh
mollifier/cd-gitroot
voronkovich/gitignore.plugin.zsh
vasyharan/zsh-brew-services
gko/listbox
johnhamelink/rvm-zsh
akoenig/gulp-autocompletion-zsh
# System
tmuxinator
#ssh-agent
axtl/gpg-agent.zsh
rupa/z
supercrabtree/k
zuxfoucault/colored-man-pages_mod
command-not-found
zsh-users/zsh-autosuggestions
zsh-users/zsh-completions
zsh-users/zsh-syntax-highlighting
zsh-users/zsh-history-substring-search
djui/alias-tips
molovo/crash
horosgrisa/zsh-dropbox
# gko/ssh-connect
wbinglee/zsh-wakatime
# sindresorhus/pretty-time-zsh
chrissicool/zsh-256color
marzocchi/zsh-notify
jreese/zsh-titles
unixorn/autoupdate-antigen.zshplugin
trapd00r/zsh-syntax-highlighting-filetypes
#zdharma/fast-syntax-highlighting
unixorn/warhol.plugin.zsh
joel-porquet/zsh-dircolors-solarized
# b4b4r07/emoji-cli
hcgraf/zsh-sudo
# Editor & iTerm
# kingsj/atom_plugin.zsh
davidtong/vsc.plugin.zsh
# tysonwolker/iterm-tab-colors
edouard-lopez/yeoman-zsh-plugin
EOBUNDLES

# Source Syntax Highlight Settings
source ~/.zsh/config.zsh

# Tell antigen that you're done.
antigen apply
