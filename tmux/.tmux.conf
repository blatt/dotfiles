
# if you're running tmux within iTerm2
#   - and tmux is 1.9 or 1.9a
#   - and iTerm2 is configured to let option key act as +Esc
#   - and iTerm2 is configured to send [1;9A -> [1;9D for option + arrow keys
# then uncomment the following line to make Meta + arrow keys mapping work
#set -ga terminal-overrides "*:kUP3=\e[1;9A,*:kDN3=\e[1;9B,*:kRIT3=\e[1;9C,*:kLFT3=\e[1;9D"

# enable mouse
#setw -g mode-mouse on

# allow mouse to select windows and panes
#set -g mouse-select-pane on
#set -g mouse-resize-pane on
#set -g mouse-select-window on

set-window-option -g allow-rename off

# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

bind q killp

# switch panes using Alt-arrow without prefix
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# split panes using | and -
bind | split-window -h
bind - split-window -v
unbind '"'
unbind %

# Rather than constraining window size to the maximum size of any client 
# connected to the *session*, constrain window size to the maximum size of any 
# client connected to *that window*. Much more reasonable.
setw -g aggressive-resize on

tmux_conf_new_pane_reconnect_ssh=true
tmux_conf_new_session_prompt=true

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf \; display "Configuration Reloaded!"

# start index at 1
set -g base-index 1
set -g pane-base-index 1

#set -g status-right "#[fg=#(uptime | cut -d ',' -f 2-)"
#set -g status-left '#{prefix_highlight} | %a, %d %b %Y %H:%M'

set -g @prefix_highlight_fg 'white' # default is 'colour231'
set -g @prefix_highlight_bg 'red'  # default is 'colour04'
set -g @prefix_highlight_output_prefix '[ '
set -g @prefix_highlight_output_suffix ' ]'

set-window-option -g status-left ' [ #S ] '
set-window-option -g status-left-fg white
set-window-option -g status-left-bg green
set-window-option -g status-right "#{uptime} | %H:%M #{prefix_highlight}  "
set-window-option -g status-right-fg white
set-window-option -g status-right-bg green
set-window-option -g window-status-format " #I: #W "
set-window-option -g window-status-current-format " #I: #W "
set-window-option -g window-status-current-fg green
set-window-option -g window-status-current-bg black

# visual notification of activity in other windows
setw -g monitor-activity on
set -g visual-activity on

# https://github.com/tmux-plugins/tpm
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'jamesoff/tmux-loadavg'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'jbnicolai/tmux-fpp'
set -g @plugin 'seebi/tmux-colors-solarized'
set -g @plugin 'robhurring/tmux-uptime'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'