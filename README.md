
![](https://img.shields.io/badge/.-files-lightgrey.svg) ![](https://img.shields.io/badge/shell-zsh-blue.svg) ![](https://img.shields.io/badge/GNU-stow-orange.svg) ![homebrew](https://img.shields.io/homebrew/v/cake.svg) ![](https://img.shields.io/badge/-MacOS-green.svg) ![](https://img.shields.io/badge/MIT-License-green.svg) 

# dotfiles

This repository contains most of my base configuration files for macOS. These files make it simple to configure a new machine from scratch as well as keep it up-to-date over time.

**WARNING:** 
This is my **personal** setup. It works for me, but isn’t necessarily optimal for you. Don't blindly run the setup script without having an understanding of how everything works! You should first fork this repository, review the code, and remove things you don’t want or need. Use at your own risk!
 

## Installation

TODO: Write installation instructions.

## How to use?

TODO: Write usage instructions

## Features

### zsh Shell

* [powerlevel9k](https://github.com/bhilburn/powerlevel9k) Theme


## Tech used
* [Stow - GNU Project - Free Software Foundation](https://www.gnu.org/software/stow/)
* [homebrew — The missing package manager for macOS](https://brew.sh/)
* [git-secret](http://git-secret.io/) for encrypting and decrypting *.secret files.

### Submodules
Contains git submodules of:

* [zsh-users/antigen: The plugin manager for zsh.](https://github.com/zsh-users/antigen) © MIT
* [tmux-plugins/tpm: Tmux Plugin Manager](https://github.com/tmux-plugins/tpm) ©  MIT

## License

© Distributed under the terms of the [MIT License](https://gitlab.com/blatt/dotfiles/raw/master/LICENSE).
